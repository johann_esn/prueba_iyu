<!--
 * Datos de autor
 * nombre: Johann Esneider
 * e-mail: johannesneider.dev@gmail.com
 * última modificación: 17 Mayo 2021
-->

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <title>Módulo radicados - IYU</title>
    <!-- inicio - Estilo general -->
    <link rel="stylesheet" href="../../Resources/css/general_style.css" style="text/css">
    <!-- fin - Estilo general -->
    <!-- inicio Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- fin Bootstrap-->
    <!-- inicio material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- fin material icons -->
    <!--  Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- fin Fonts -->
</head>
<body class="content">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../../index.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><b>Radicados</b></li>
        </ol>
    </nav>
    <section class="container" style="text-align:center;">
    <fieldset>
        <legend>Módulo de radicados</legend>
        <br>
        <div class="form-group">
            <label for="buscar">Buscar por número de radicado</label>
            <input type="number" min="1" max="14" class="form-control" name="txt_radicado" id="txt_radicado" placeholder="Digite número de radicado" />
        </div>
        <div class="form-group">
            <input type="button" class="btn btn-primary btn-block" id="search_btn" value="Buscar radicado" onClick="search_rad();">
        </div>
        <table class="table table-bordered table-sm" id="radicado_datatable">
            <thead>
                <tr>
                    <th colspan="4">Datos del radicado</th>
                    <th colspan="3">Datos del remitente</th>
                    <th rowspan="2">Acción</th>
                </tr>
                <tr>
                    <th>Fecha radicado</th>
                    <th>Asunto</th>
                    <th>Tipo documento</th>
                    <th>Número de radicado</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                </tr>
            </thead>
            <tbody id="radicado_tab"></tbody>
        </table>
    </fieldset>
        <!-- Modal -->
        <div class="modal fade" id="radicado_modal" tabindex="-1" role="dialog" aria-labelledby="radicado_modal"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <h5 class="modal-title" id="exampleModalLabel"><b>Modificar registro</b></h5>
                    </div>
                    <div class="modal-body">
                        <form name="radicado_form" id="radicado_form">
                            <div class="form-row">
                                <div class="col">
                                    <label for="f_radicado"><b>Fecha radicado</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="f_radicado" name="f_radicado" readonly />
                                </div>
                                <div class="col">
                                    <label for="asunto"><b>Asunto</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="asunto" name="asunto" placeholder="Digite el asunto del radicado" />
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                    <label for="t_document"><b>Tipo documento</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="t_document" name="t_document" readonly />
                                </div>
                                <div class="col">
                                    <label for="t_remitente"><b>Teléfono remitente</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="t_remitente" name="t_remitente" readonly />
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                    <label for="n_remitente"><b>Nombre remitente</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="n_remitente" name="n_remitente" readonly />
                                </div>
                                <div class="col">
                                    <label for="d_remitente"><b>Dirección remitente</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="d_remitente" name="d_remitente" readonly />
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="n_radicado"><b>Número de radicado</b></label>
                                <input type="text" style="text-align: center;" class="form-control" id="n_radicado" name="n_radicado" readonly />
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="id_rad" name="id_rad" />
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <input type="button" class="btn btn-info" value="Actualizar registro" onClick="save_data();" />
                    </div>
                </div>
            </div>
        </div>
        <br>
        <footer>
            <small class="footer">Developed by Johann Esneider&copy; - Prueba técnica IYU</small>
        </footer>
    </section>
    <!-- inicio Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- fin Bootstrap -->
    <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- fin jQuery -->
    <!-- Inicio Sweet alert 2-->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!-- Fin Sweet alert 2-->
    <!-- Inicio Datatable-->
    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"></script>
    <!-- Fin Datatable-->
    <script src="../../Resources/js/radicado.js" type="text/javascript"></script>
</body>
</html>