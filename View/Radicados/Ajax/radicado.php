<?php

session_start();
class ajax
{

    // conexión a la base
    public function conectar()
    {
        $file_conn="../../../Config/config.ini";
		$conn_data = parse_ini_file($file_conn,true);

        $conn_string = "host=".$conn_data["host"]." port=".$conn_data["port"]." dbname=".$conn_data["database"]." user=".$conn_data["user"]." password=".$conn_data["password"]." options='--client_encoding=UTF8'";
        $dbconn = pg_connect($conn_string);

        return $dbconn;
    }

    public function get_radicado($number){

        $conn = $this->conectar();

        $data = [];

        $sql = "SELECT * FROM radicado WHERE radi_nume_radi = '".$number."' ";
        $res = pg_query($conn, $sql);
        while($row = pg_fetch_array($res)){

            $sql_remitente = "SELECT * FROM sgd_dir_drecciones WHERE radi_nume_radi = '".$row['radi_nume_radi']."'";
            $res_remitente = pg_query($conn, $sql_remitente);
            while($row_remitente = pg_fetch_array($res_remitente)){

                $row['r_nombre']    = $row_remitente['sgd_dir_nomremdes'];
                $row['r_direccion'] = $row_remitente['sgd_dir_direccion'];
                $row['r_telefono']  = $row_remitente['sgd_dir_telefono'];

                $sql_document_type = "SELECT sgd_tpr_descrip FROM sgd_tpr_tpdcumento WHERE sgd_tpr_codigo = '".$row['tdoc_codi']."'";
                $res_document_type = pg_query($conn, $sql_document_type);
                $row_document_type = pg_fetch_array($res_document_type);

                $row['document_type'] = $row_document_type['sgd_tpr_descrip'];
            }

            $data[] = $row;
        }
        
        return $data;
    }

    public function edit_radicado($id){
        $conn = $this->conectar();

        $data = [];

        $sql = "SELECT * FROM radicado WHERE id = $id";
        $res = pg_query($conn, $sql);
        while($row = pg_fetch_assoc($res)){

            $sql_remitente = "SELECT * FROM sgd_dir_drecciones WHERE radi_nume_radi = ".$row['radi_nume_radi']."";
            $res_remitente = pg_query($conn, $sql_remitente);
            $row_remitente = pg_fetch_assoc($res_remitente);

            $row['r_nombre']    = $row_remitente['sgd_dir_nomremdes'];
            $row['r_direccion'] = $row_remitente['sgd_dir_direccion'];
            $row['r_telefono']  = $row_remitente['sgd_dir_telefono'];

            $sql_document_type = "SELECT sgd_tpr_descrip FROM sgd_tpr_tpdcumento WHERE sgd_tpr_codigo = ".$row['tdoc_codi']."";
            $res_document_type = pg_query($conn, $sql_document_type);
            $row_document_type = pg_fetch_assoc($res_document_type);

            $row['document_type'] = $row_document_type['sgd_tpr_descrip'];

            $data[] = $row;
        }

        return $data;
    }

    public function update_radicado(){
        $conn = $this->conectar();

        $state = 0;

        $sql = "UPDATE radicado SET ra_asun = '".$_POST['asunto']."' WHERE id = '".$_POST['id_rad']."' ";
        $res = pg_query($conn, $sql);

        if($res){
            $state = 2;
        }

        return $state;
    }
}

//Crea el objeto de la clase ajax
$obj = new ajax;

//Evalúa depende la petición del scrip "Accion=  (lo que venga en la petición)"
if (isset($_POST)) {

    $action = $_REQUEST['action'];

    if ($action == 'get_radicado') {
        
        $detail = $obj->get_radicado($_POST['radicado']);
        echo json_encode($detail);

    }else if($action == 'edit'){

        $detail = $obj->edit_radicado($_POST['Id']);
        echo json_encode($detail);

    }else if($action == 'update'){
        $state = $obj->update_radicado();
        echo $state;
    }

}