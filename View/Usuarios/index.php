<!--
 * Datos de autor
 * nombre: Johann Esneider
 * e-mail: johannesneider.dev@gmail.com
 * última modificación: 17 Mayo 2021
-->

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <title>Módulo usuarios - IYU</title>
    <!-- inicio - Estilo general -->
    <link rel="stylesheet" href="../../Resources/css/general_style.css" style="text/css">
    <!-- fin - Estilo general -->
    <!-- inicio Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- fin Bootstrap-->
    <!-- inicio material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- fin material icons -->
    <!--  Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- fin Fonts -->
</head>
<body class="content">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../../index.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><b>Usuarios</b></li>
        </ol>
    </nav>
    <section class="container" style="text-align:center;">
        <fieldset>
            <legend>Módulo de usuarios</legend>
            <br>
            <div class="form-group">
                <label for="buscar">Buscar por número de identificación</label>
                <input type="number" min="1" max="14" class="form-control" name="txt_usuario" id="txt_usuario" placeholder="Digite número de identificación" />
            </div>
            <div class="form-group">
                <input type="button" class="btn btn-primary btn-block" id="search_btn" value="Buscar usuario" onClick="search_usu();">
            </div>
            <table class="table table-bordered table-sm" id="user_datatable">
                <thead>
                    <tr>
                        <th colspan="6">Datos del usuario</th>
                        <th rowspan="2">Acción</th>
                    </tr>
                    <tr>
                        <th>Nombre</th>
                        <th>Nombre de usuario</th>
                        <th>Código dependencia</th>
                        <th>Perfil</th>
                        <th>Estado</th>
                        <th>Correo electrónico</th>
                    </tr>
                </thead>
                <tbody id="usuario_tab"></tbody>
            </table>
        </fieldset>

        <!-- Modal -->
        <div class="modal fade" id="usuario_modal" tabindex="-1" role="dialog" aria-labelledby="radicado_modal"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <h5 class="modal-title" id="exampleModalLabel"><b>Modificar registro</b></h5>
                    </div>
                    <div class="modal-body">
                        <form name="user_form" id="user_form">
                            <div class="form-row">
                                <div class="col">
                                    <label for="name"><b>Nombre</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="name" name="name" readonly />
                                </div>
                                <div class="col">
                                    <label for="username"><b>Nombre de usuario</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="username" name="username" readonly />
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                    <label for="cod_dep"><b>Código dependencia</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="cod_dep" name="cod_dep" readonly />
                                </div>
                                <div class="col">
                                    <label for="cod_usu"><b>Código usuario</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="cod_usu" name="cod_usu" readonly />
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                    <label for="estado_usu"><b>Estado</b></label>
                                    <input type="text" style="text-align: center;" class="form-control" id="estado_usu" name="estado_usu" readonly />
                                </div>
                                <div class="col">
                                    <label for="email_usu"><b>Correo electrónico</b></label>
                                    <input type="email" style="text-align: center;" class="form-control" id="email_usu" name="email_usu" placeholder="Digite correo electrónico" />
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <input type="hidden" id="id_usu" name="id_usu" value="" />
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <input type="button" class="btn btn-info" value="Actualizar registro" onClick="save_data();" />
                    </div>
                </div>
            </div>
        </div>
        <br>
        <footer>
            <small class="footer">Developed by Johann Esneider&copy; - Prueba técnica IYU</small>
        </footer>
    </section>
    <!-- inicio Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- fin Bootstrap -->
    <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- fin jQuery -->
    <!-- Inicio Sweet alert 2-->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!-- Fin Sweet alert 2-->
    <!-- Inicio Datatable-->
    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"></script>
    <!-- Fin Datatable-->
    <script src="../../Resources/js/usuario.js" type="text/javascript"></script>
</body>
</html>