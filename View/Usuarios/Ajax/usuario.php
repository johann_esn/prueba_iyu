<?php

session_start();
class ajax
{

    // conexión a la base postgresql.
    public function conectar()
    {
        $file_conn="../../../Config/config.ini";
		$conn_data = parse_ini_file($file_conn,true);

        $conn_string = "host=".$conn_data["host"]." port=".$conn_data["port"]." dbname=".$conn_data["database"]." user=".$conn_data["user"]." password=".$conn_data["password"]." options='--client_encoding=UTF8'";
        $dbconn = pg_connect($conn_string);

        return $dbconn;
    }
    // función que obtiene los datos del usuario que correspondan al número de identificacion recibido por ajax.
    public function get_user($doc){

        $conn = $this->conectar();

        $data = [];

        $sql = "SELECT * FROM usuario WHERE usua_doc = '".$doc."' ";
        $res = pg_query($conn, $sql);
        while($row = pg_fetch_assoc($res)){

            $sql_dep = "SELECT depe_nomb FROM dependencia WHERE depe_codi = '".$row['depe_codi']."'";
            $res_dep = pg_query($conn, $sql_dep);
            $row_dep = pg_fetch_assoc($res_dep);

            if($row['usua_esta'] == '1'){
                $row['user_state'] = 'Activo';
            }else if($row['usua_esta'] == 0){
                $row['user_state'] = 'Inactivo';
            }

            if($row['usua_codi'] == '1'){
                $row['perfil'] = 'Jefe';
            }else{
                $row['perfil'] = 'Normal';
            }
            
            $row['nombre_dependencia'] = $row_dep['depe_nomb'];

            $data[] = $row;
        }
        
        return $data;
    }
    // función que obtiene los datos del usuario al dar clic en editar(trae los datos por id).
    public function edit($id){
        $conn = $this->conectar();

        $data = [];

        $sql = "SELECT * FROM usuario WHERE id = '".$id."' ";
        $res = pg_query($conn, $sql);
        while($row = pg_fetch_assoc($res)){

            $sql_dep = "SELECT depe_nomb FROM dependencia WHERE depe_codi = '".$row['depe_codi']."'";
            $res_dep = pg_query($conn, $sql_dep);
            $row_dep = pg_fetch_assoc($res_dep);

            if($row['usua_esta'] == 1){
                $row['user_state'] = 'Activo';
            }else if($row['usua_esta'] == 0){
                $row['user_state'] = 'Inactivo';
            }

            if($row['usua_codi'] == '1'){
                $row['perfil'] = 'Jefe';
            }else{
                $row['perfil'] = 'Normal';
            }
            
            $row['nombre_dependencia'] = $row_dep['depe_nomb'];

            $data[] = $row;
        }
        
        return $data;
    }
    // actualiza el campo usua_email de la base de datos donde el id es igual al que recibe como parametro.
    public function update(){
        $conn = $this->conectar();
        $state = 0;

        $sql = "UPDATE usuario SET usua_email = '".$_POST['email_usu']."' WHERE id = '".$_POST['id_usu']."' ";
        $res = pg_query($conn, $sql);

        if($res){
            $state = 2;
        }

        return $state;
    }
}

//Crea el objeto de la clase ajax
$obj = new ajax;

//Evalúa depende la petición del scrip "Accion=  (lo que venga en la petición)"
if (isset($_POST)) {

    $action = $_REQUEST['action'];

    if ($action == 'get_user') {
        $detail = $obj->get_user($_POST['document']);
        echo json_encode($detail);
    }else if($action == 'edit'){
        $detail = $obj->edit($_POST['Id']);
        echo json_encode($detail);
    }else if($action == 'update'){
        $state = $obj->update();
        echo $state;
    }
}